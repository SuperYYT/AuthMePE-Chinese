# AuthMePE
Bukkit的AuthMe插件在PocketMine上的重置

[![Poggit-CI](https://poggit.pmmp.io/ci.badge/Edwardthedog2/AuthMePE/AuthMePE)](https://poggit.pmmp.io/ci/Edwardthedog2/AuthMePE/AuthMePE)

# 特性:

1. 注册/登录/更换密码

2. 远程连接信息

3. 输入密码到聊天登录

4. 先进的注册系统

5. 音效支持

6. 配置支持-完全可定制

7. 自定义事件,为开发者

8. API, 为开发者

9. 管理指令 - 让管理员以不同的方式管理用户

10. 防止未登录玩家做动作。聊天/命令/位置/移动

11. 防止玩家说出他们的密码

12. Hash保护 - md5 & sha256, 超级salt支持

13. 'Specter' 插件支持

14. IP地址记录支持

15. 玩家的登录次数最多.

16. 令牌系统 - 在控制台生成令牌,
    凭借令牌登入任何帐号
    (这个系统是超级安全的XD)

 **注：原插件可能出现每次登陆自动返回主城的问题，这是设置文件的原因，
请大家下载最新版本或者在配置文件中将force-spawn选项改为false即可** 