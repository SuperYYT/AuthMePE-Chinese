<?php

namespace AuthMePE;

use pocketmine\scheduler\PluginTask;
use AuthMePE\AuthMePE;
use pocketmine\utils\TextFormat;
use pocketmine\level\sound\PopSound;

class Task extends PluginTask{
	public $plugin;
	
	public function __construct(AuthMePE $plugin){
		$this->plugin = $plugin;
		parent::__construct($plugin);
	}
	
	public function onRun($tick){
		foreach($this->plugin->getServer()->getOnlinePlayers() as $p){
			if($this->plugin->isRegistered($p) && !$this->plugin->isLoggedIn($p)){
				$p->sendMessage("请在聊天框中输入你的密码来登录。");
				$p->sendPopup(TextFormat::GOLD."欢迎 ".TextFormat::AQUA.$p->getName().TextFormat::GREEN."\n请登录后开始游戏!");
				$p->getLevel()->addSound(new PopSound($p), $this->plugin->getServer()->getOnlinePlayers());
			}
		}
	}
}